package com.example.healthone;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aditya on 13-10-2016.
 */
public class PatientInfoAdapter extends ArrayAdapter {

List list=new ArrayList<>();
    public PatientInfoAdapter(Context context, int resource) {
        super(context, resource);
    }

    static class DataHandler
    {
        TextView column;
        TextView value;
    };

       @Override
    public void add(Object object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public Object getItem(int position){
        return this.list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row=convertView;
        DataHandler handler;
        if(convertView==null){

                LayoutInflater layoutInflater=(LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row=layoutInflater.inflate(R.layout.patient_info_each,parent,false);
            handler=new DataHandler();
            handler.column=(TextView) row.findViewById(R.id.column);
            handler.value=(TextView) row.findViewById(R.id.value);
            row.setTag(handler);

        }
        else{
            handler=(DataHandler) row.getTag();
        }

        PatientInfo patientInfo;
        patientInfo=(PatientInfo) this.getItem(position);
        handler.column.setText(patientInfo.getColumn());
        handler.value.setText(patientInfo.getValue());

        return row;
    }
}









