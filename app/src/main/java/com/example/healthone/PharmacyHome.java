package com.example.healthone;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Lohitt on 16-10-2016.
 */
public class PharmacyHome extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pharmacy_home);
    }
}
