package com.example.healthone;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableQueryCallback;

import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Lohitt on 16-10-2016.
 */
public class PatientPharmacy extends Activity {
    MobileServiceClient mClient;
    MobileServiceTable<Prescription> mPrescriptionTable;
    String patientID;
    TextView date;
    TextView doctorID;
    TextView medicine;
    TextView quantity;

    String date1;
    String doctorID1;
    String medicine1;
    String quantity1;
    DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patientprescription);

        try {
            mClient = new MobileServiceClient(
                    "https://healthone.azurewebsites.net",
                    this
            );
        } catch (MalformedURLException me) {

            me.getStackTrace();
        }
        mPrescriptionTable = mClient.getTable(Prescription.class);
        Intent intent = getIntent();
        patientID=intent.getStringExtra("patientID");
        readLatestPrescription();
    }


    public void readLatestPrescription() {

        try {
            mPrescriptionTable.where().field("patient_id").eq(patientID)
                    .execute(new TableQueryCallback<Prescription>() {
                        public void onCompleted(List<Prescription> result,
                                                int count,
                                                Exception exception,
                                                ServiceFilterResponse response) {
                            if (exception == null) {
                                for (Prescription p : result) {
                                    date1=dateFormat.format(p.getUpdatedAt());
                                    doctorID1=p.getDoctor_id();
                                    medicine1=p.getMedicine_id();
                                    quantity1=p.getQuantity();
                                    populateTextField();
                                    /*values.add("008");
                                    values.add("Lohitt");
                                    values.add("B S");
                                    values.add("24");
                                    values.add("124115323");
                                    values.add("unilodge");
                                    values.add("O positive");*/


                                }
                            } else {
                                Context context = getApplicationContext();
                                //   Toast.makeText(Doctor.this, "Ops!!! Error.", 1000).show();
                                Toast toast = Toast.makeText(context, "incorrect password", Toast.LENGTH_LONG);
                            }
                        }
                    });

        }catch (Exception e){
            Log.v("exception",e.getStackTrace().toString());
        }

    }

    public void populateTextField(){
        date=(TextView) findViewById(R.id.datePrescription);
        doctorID=(TextView)findViewById(R.id.doctorPrescription);
        medicine=(TextView)findViewById(R.id.medicines);
        quantity=(TextView) findViewById(R.id.quantity);
        date.setText(date1);
        doctorID.setText(doctorID1);
        medicine.setText(medicine1);
        quantity.setText(quantity1);

    }

    public void onSendToPharmacyClick(View view){
        Intent data=new Intent(this,DisplayPharmacies.class);
        data.putExtra("patientID",patientID);
        startActivity(data);
    }
}
