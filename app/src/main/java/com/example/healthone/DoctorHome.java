package com.example.healthone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

/**
 * Created by Aditya on 11-10-2016.
 */
public class DoctorHome extends Activity{
String doctorID;
    EditText patientIDET;
    public void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_doctor_home);
        Intent data=getIntent();
        doctorID=data.getStringExtra("doctorID");


    }

    public void onLoginClick(View view){

        patientIDET=(EditText) findViewById(R.id.patientID);
        Intent data=new Intent(this,DoctorPatientDetails.class);
        data.putExtra("patientId",patientIDET.getText().toString());
        data.putExtra("doctorID",doctorID);
        setResult(RESULT_OK);
        startActivity(data);



    }
}
