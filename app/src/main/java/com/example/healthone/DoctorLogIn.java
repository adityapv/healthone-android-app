package com.example.healthone;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceException;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableOperationCallback;
import com.microsoft.windowsazure.mobileservices.table.TableQueryCallback;

import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Aditya on 11-10-2016.
 */
public class DoctorLogIn extends Activity {

    EditText doctorIDET;
    EditText passwordET;
    String doctorpwd;
    String doctorID;
    boolean flag=false;
    MobileServiceClient mClient;
    MobileServiceTable<Doctor> mDoctorTable;
    public void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_doctor_login);
      try {
          mClient = new MobileServiceClient(
                  "https://healthone.azurewebsites.net",
                  this
          );
      }catch (MalformedURLException me)
      {
          me.getStackTrace();
      }

        mDoctorTable = mClient.getTable(Doctor.class);
         /*Doctor item = new Doctor();
        item.doctor_id = "1414153";
        item.address="25 Larkin St";
        item.contact_number="0420427377";
        item.first="sagar";
        item.last="Srinivas";
        item.email="sagarsrinivas94@gmail.com";
        item.password="qwerty";
                    mClient.getTable(Doctor.class).insert(item, new TableOperationCallback<Doctor>() {
                        public void onCompleted(Doctor entity, Exception exception, ServiceFilterResponse response) {
                            if (exception == null) {

                            } else {
                                // Insert failed
                    System.out.println(exception.getStackTrace());
                                System.out.println(exception.getMessage());
                }
            }
        });
*/

    }

    public void onLoginClick(View view) throws Exception
    {
        doctorIDET=(EditText)findViewById(R.id.login1);
        passwordET=(EditText)findViewById(R.id.password);
        doctorpwd=passwordET.getText().toString();
       doctorID=doctorIDET.getText().toString();

        System.out.println("****************************************"+doctorpwd+"***********************************");
        System.out.println("****************************************"+doctorID+"***********************************");
        Context context=getApplicationContext();
        Toast toast = Toast.makeText(context, doctorID, Toast.LENGTH_LONG);
      // validateLogin(doctorIDET.toString(),passwordET.toString());



       mDoctorTable.where().field("doctor_id").eq(doctorID).select("password")
                .execute(new TableQueryCallback<Doctor>() {
                    public void onCompleted(List<Doctor> result,
                                            int count,
                                            Exception exception,
                                            ServiceFilterResponse response) {
                        if (exception == null) {
                            for(Doctor d:result){

                                    if(doctorpwd.equals(d.password)){
                                        flag=true;
                                        gotoNextPage();
                                        break;


                                    }

                            }
                        }  else{
                            Context context=getApplicationContext();
                         //   Toast.makeText(Doctor.this, "Ops!!! Error.", 1000).show();

                            System.out.println(exception.getStackTrace());
                            Toast toast = Toast.makeText(context, "incorrect password", Toast.LENGTH_LONG);
                        }}
                });

    }

    public void gotoNextPage(){
        Intent intent1= new Intent(this,DoctorHome.class);
        intent1.putExtra("doctorID",doctorID);
        setResult(RESULT_OK);
        startActivity(intent1);
    }

    public void onSignUpClick(View view){


    }
    public List<Doctor> readDoctorInfo(String doctorID) throws Exception {

        final    List<Doctor> doctorList= mClient.getTable(Doctor.class).where().field("doctor_id").eq(doctorID).select("password").execute().get();
       // mDoctorTable.where().field("doctor_id").eq(doctorID).select("password").execute().get();
       //    final    List<Doctor> doctorList= mDoctorTable.execute().get();
       // = mDoctorTable.where().field("doctor_id").eq(doctorID).select("password").execute().get();

       // List<Doctor> doctorList = mDoctorTable.execute().get();
        return doctorList;
    }

    public void validateLogin(String doctorID, String password){
        Context context=getApplicationContext();
        Intent intent=new Intent(DoctorLogIn.this,DoctorHome.class);
     try {
       //  MobileServiceList<Doctor> lst =  mClient.getTable(Doctor.class).where().field("doctor_id").eq("1414153").select("password").execute().get();
         List<Doctor> lst = mClient.getTable(Doctor.class).where().field("doctor_id").eq(doctorID).execute().get();
         Log.v("abc", lst.toString());
         if (password.equals(lst.get(0))) {
             startActivity(intent);
         } else {
             Toast toast = Toast.makeText(context, "incorrect password", Toast.LENGTH_LONG);
         }
     }catch(Exception e){
      System.out.println(e.getStackTrace());
     }


    }
}
