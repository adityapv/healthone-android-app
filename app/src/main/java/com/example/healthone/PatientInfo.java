package com.example.healthone;

/**
 * Created by Aditya on 13-10-2016.
 */
public class PatientInfo {

    public String column;
    public String value;

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public PatientInfo() {
    }

    public PatientInfo(String column, String value) {
        this.column = column;
        this.value = value;
    }
}
