package com.example.healthone;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableQueryCallback;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Aditya on 14-10-2016.
 */
public class PatientVisitSummary extends Activity {


    ArrayList<Date> dates=new ArrayList<>();
    ArrayList<String> doctorIDs= new ArrayList<>();
    ArrayList<String> reasons=new ArrayList<>();
    ArrayList<String> symptoms= new ArrayList<>();
    MobileServiceTable<Patient_Visits> mVisitTable;
    MobileServiceClient mClient;
    ListView listView;
    VisitAdapter adapter;
    String patientID;
    List<Patient_Visits> pv;
    String flag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patient_visit_summary);
        Intent getData=getIntent();
        patientID=getData.getStringExtra("patientID");





        try {
            mClient = new MobileServiceClient(
                    "https://healthone.azurewebsites.net",
                    this
            );
        } catch (MalformedURLException me) {
            me.getStackTrace();
        }
        mVisitTable=mClient.getTable("patient_visits",Patient_Visits.class);

           readVisitInfo();



    /*    try {
            pv =  mVisitTable.where().field("patient_id").eq(patientID).execute().get();
        }catch(Exception e){
            Log.v("Exc",e.getStackTrace().toString());
        }
        for(Patient_Visits p:pv){

            doctorIDs.add(p.getDoctor_id());
            reasons.add(p.getReason_for_visit());
            symptoms.add(p.getSymptoms());
            dates.add(p.getVisit_date());
        }*/
    listView=(ListView)findViewById(R.id.visitlistView);
       adapter=new VisitAdapter(getApplicationContext(),R.layout.visits_layout);
        listView.setAdapter(adapter);


    }

    public void readVisitInfo(){


        try {
           mVisitTable.where().field("patient_id").eq(patientID)
                    .execute(new TableQueryCallback<Patient_Visits>() {
                        public void onCompleted(List<Patient_Visits> result,
                                                int count,
                                                Exception exception,
                                                ServiceFilterResponse response) {
                            if (exception == null) {
                                for (Patient_Visits p : result) {
                                    doctorIDs.add(p.doctor_id);
                                    reasons.add(p.reason_for_visit);
                                    symptoms.add(p.symptoms);
                                    dates.add(p.visit_date);
                                    display();
                                    /*values.add("008");
                                    values.add("Lohitt");
                                    values.add("B S");
                                    values.add("24");
                                    values.add("124115323");
                                    values.add("unilodge");
                                    values.add("O positive");*/


                                }
                            } else {
                                Context context = getApplicationContext();
                                //   Toast.makeText(Doctor.this, "Ops!!! Error.", 1000).show();
                                Toast toast = Toast.makeText(context, "incorrect password", Toast.LENGTH_LONG);
                            }
                        }
                    });

        }catch (Exception e){
            Log.v("exception",e.getStackTrace().toString());
        }
    }

    public void display(){
        int i=0;
        for(String reason:reasons){
            Patient_Visits p = new Patient_Visits(doctorIDs.get(i),reasons.get(i),symptoms.get(i),dates.get(i));
            adapter.add(p);
            i++;
        }
    }

}
