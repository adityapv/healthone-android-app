package com.example.healthone;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.TableQueryCallback;

import java.net.MalformedURLException;
import java.util.List;

/**
 * Created by Lohitt on 13-10-2016.
 */
public class PatientLogIn extends Activity{

    boolean flag = false;
    EditText patientIDET;
    EditText patPasswordET;

    MobileServiceClient mPatClient;


    public void onCreate(Bundle savedPatientInstance){
        super.onCreate(savedPatientInstance);
        setContentView(R.layout.activity_patient_login);
        try {
            mPatClient = new MobileServiceClient(

                    "https://healthone.azurewebsites.net",
                    this
            );
        }catch (MalformedURLException me)
        {
            me.getStackTrace();
        }
        /*MobileServiceTable<Patient> mPatientTable = mPatClient.getTable(Patient.class);
        Patient item = new Patient();
        item.id="002";
        item.patientId = "009";
        item.password = "qwerty";
        mPatientTable.insert(item, new TableOperationCallback<Patient>() {
            public void onCompleted(Patient entity, Exception exception, ServiceFilterResponse response) {
                if (exception == null) {

                } else {
                    // Insert failed
                    System.out.println(exception.getStackTrace());
                    System.out.println(exception.getMessage());
                }
            }

        });*/



    }

    public void onPatientClick(View view) throws Exception
    {
        patientIDET=(EditText)findViewById(R.id.loginp);
        Log.v("Text", patientIDET.getText().toString());
        patPasswordET=(EditText)findViewById(R.id.passwordp);
        readPatientInfo(patientIDET.getText().toString());
        //validateLogin(patientIDET.getText().toString(),patPasswordET.getText().toString());
        System.out.println(patientIDET+"********************************************");

    }
    public void onSignUpClick(View view){


    }

    public int readPatientInfo(String patientID) throws Exception {
        //MobileServiceTable<Patient> mPatientTable = mPatClient.getTable(Patient.class);
        final String patPassword = patPasswordET.getText().toString();

        mPatClient.getTable(Patient.class).where().field("patientId").eq(patientID).select("password").execute(new TableQueryCallback<Patient>() {
            @Override
            public void onCompleted(List<Patient> result, int count, Exception exception, ServiceFilterResponse response) {

                if (exception == null){
                    for(Patient p:result){
                        if(patPassword.equals(p.password)){
                                    flag=true;
                                    gotoNextPage();
                                    break;
                                }
                            }
                        }

                        else{
                                //Toast.makeText(context , "Ops!!! Error.", 1000).show();
                                Context context = getApplicationContext();
                                int duration= Toast.LENGTH_SHORT;
                                Toast.makeText(context, "wrong password", duration).show();
                        }}
                    });
        return 0;
    }

    public void gotoNextPage(){
        Intent intent= new Intent(PatientLogIn.this ,PatientHome.class);
        intent.putExtra("patientID",patientIDET.getText().toString());
        startActivity(intent);
    }
}


