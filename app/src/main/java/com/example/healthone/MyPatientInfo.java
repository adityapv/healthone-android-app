package com.example.healthone;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableQueryCallback;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lohitt on 16-10-2016.
 */
public class MyPatientInfo  extends Activity{
    ListView listView;
    MobileServiceClient mClient;
    ArrayAdapter<String> itemsAdapter;
    ArrayList<String> values=new ArrayList<>();
    ArrayList<String> columns=new ArrayList<>();
    MobileServiceTable<Patient> mPatientTable;
    MyPatientInfo myPatientInfo;
    MyInfoAdapter adapter;
    String doctorID;

    String patientID;
    public void onCreate(Bundle savedInstance){

        super.onCreate(savedInstance);
        setContentView(R.layout.patient_doctor_layout);
        try {
            mClient = new MobileServiceClient(
                    "https://healthone.azurewebsites.net",
                    this
            );
        }catch (MalformedURLException me)
        {
            me.getStackTrace();
        }
        listView=(ListView) findViewById(R.id.listview1);
    //    adapter=new PatientInfoAdapter(getApplicationContext(),R.layout.patient_info_each);
        try {
            listView.setAdapter(adapter);
        }catch (Exception e){
            Log.v("Exception",e.getStackTrace().toString());
        }
        mPatientTable=mClient.getTable(Patient.class);
        Intent getData=getIntent();
        patientID=getData.getStringExtra("patientId");
        doctorID=getData.getStringExtra("doctorID");






        readPatients(patientID);

    }


    void readPatients(String patientID){
        try {
            mPatientTable.where().field("patientId").eq(patientID)
                    .execute(new TableQueryCallback<Patient>() {
                        public void onCompleted(List<Patient> result,
                                                int count,
                                                Exception exception,
                                                ServiceFilterResponse response) {
                            if (exception == null) {
                                for (Patient p : result) {
                                    values.add(p.patientId);
                                    values.add(p.first);
                                    values.add(p.last);
                                    values.add(p.age);
                                    values.add(p.contact_number);
                                    values.add(p.address);
                                    values.add(p.blood_group);
                                    display();
                                    /*values.add("008");
                                    values.add("Lohitt");
                                    values.add("B S");
                                    values.add("24");
                                    values.add("124115323");
                                    values.add("unilodge");
                                    values.add("O positive");*/


                                }
                            } else {
                                Context context = getApplicationContext();
                                //   Toast.makeText(Doctor.this, "Ops!!! Error.", 1000).show();
                                Toast toast = Toast.makeText(context, "incorrect password", Toast.LENGTH_LONG);
                            }
                        }
                    });
            columns.add("Patient ID :");
            columns.add("First Name : ");
            columns.add("Last Name : ");
            columns.add("age : ");
            columns.add("contact Number : ");
            columns.add("address : ");
            columns.add("Blood Group : ");
        }catch (Exception e){
            Log.v("exception",e.getStackTrace().toString());
        }
    }

    public void onPatientClick(View view){
        Intent data=new Intent(this,PatientVisitSummary.class);
        data.putExtra("patientID",patientID);
        setResult(RESULT_OK);
        startActivity(data);

    }

    public void onNewEntryClick(View view){
        Intent i=new Intent(this,DoctorNotes.class);
        i.putExtra("patientID",patientID);
        i.putExtra("doctorID",doctorID);
        setResult(RESULT_OK);
        startActivity(i);

    }

    void display(){
        int i=0;
        for(String value:values){
            PatientInfo p=new PatientInfo(columns.get(i),value);
            adapter.add(p);
            i++;
        }
    }
}
