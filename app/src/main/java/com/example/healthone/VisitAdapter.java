package com.example.healthone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Aditya on 14-10-2016.
 */
public class VisitAdapter extends ArrayAdapter {

    List list=new ArrayList<>();

    DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy hh:mm:ss");
    public VisitAdapter(Context context, int resource) {
        super(context, resource);



    }
    static class DataHandler
    {

        TextView doctorID;
        TextView reason;
        TextView symptoms;
        TextView date;
    };

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public Object getItem(int position){
        return this.list.get(position);
    }
    @Override
    public void add(Object object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row=convertView;
        DataHandler handler;
        if(convertView==null){

            LayoutInflater layoutInflater=(LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row=layoutInflater.inflate(R.layout.visits_layout,parent,false);
            handler=new DataHandler();
         //   handler.column=(TextView) row.findViewById(R.id.column);
            handler.doctorID=(TextView) row.findViewById(R.id.doctor);
            handler.reason=(TextView) row.findViewById(R.id.reason);
            handler.symptoms=(TextView) row.findViewById(R.id.symptoms);
            handler.date=(TextView) row.findViewById(R.id.date);

            row.setTag(handler);

        }
        else{
            handler=(DataHandler) row.getTag();
        }

        Patient_Visits patientInfo=new Patient_Visits();
        patientInfo=(Patient_Visits) this.getItem(position);
      //  handler.column.setText(patientInfo.getColumn());
        handler.doctorID.setText(patientInfo.getDoctor_id());
        handler.reason.setText(patientInfo.getReason_for_visit());
        handler.symptoms.setText(patientInfo.getSymptoms());
        handler.date.setText(dateFormat.format(patientInfo.getVisit_date()));

        return row;
    }

}
