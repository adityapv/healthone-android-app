package com.example.healthone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Lohitt on 13-10-2016.
 */
public class PatientHome extends Activity{
    String patientID;
    public void onCreate(Bundle savedPatientInstance) {
        super.onCreate(savedPatientInstance);
        setContentView(R.layout.activity_patient_home);
        Intent intent=getIntent();
        patientID=intent.getStringExtra("patientID");



    }


    public void onPatientVisitclick(View view){

        Intent intent=new Intent(this,PatientVisitSummary.class);
        intent.putExtra("patientID",patientID);
        startActivity(intent);
    }
    public void onPharmacyClick(View view){
        Intent intent=new Intent(this,PatientPharmacy.class);
        intent.putExtra("patientID",patientID);
        startActivity(intent);
    }
}
