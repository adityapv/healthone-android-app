package com.example.healthone;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lohitt on 16-10-2016.
 */
public class MyInfoAdapter extends ArrayAdapter {
    List list=new ArrayList<>();
    public MyInfoAdapter(Context context, int resource) {
        super(context, resource);
    }

    static class DataHandler
    {
        TextView column1;
        TextView value1;
    };

    @Override
    public void add(Object object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public Object getItem(int position){
        return this.list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row=convertView;
        DataHandler handler;
        if(convertView==null){

            LayoutInflater layoutInflater=(LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row=layoutInflater.inflate(R.layout.doctor_info_each,parent,false);
            handler=new DataHandler();
            handler.column1=(TextView) row.findViewById(R.id.column);
            handler.value1=(TextView) row.findViewById(R.id.value);
            row.setTag(handler);

        }
        else{
            handler=(DataHandler) row.getTag();
        }

        MyPatientInfo myPatientInfo;
        myPatientInfo =(MyPatientInfo) this.getItem(position);
        /*handler.column1.setText(myPatientInfo.getColumn());
        handler.value1.setText(myPatientInfo.getValue());*/

        return row;
    }
}
