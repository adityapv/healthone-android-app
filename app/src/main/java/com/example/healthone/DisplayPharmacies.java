package com.example.healthone;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableQueryCallback;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lohitt on 16-10-2016.
 */
public class DisplayPharmacies extends Activity {


    MobileServiceClient mClient;
    MobileServiceTable<Pharmacy> mPharmacyTable;
    String patientID;
    ArrayList<String> pharmacyNames=new ArrayList<>();
    ArrayAdapter<String> itemsAdapter;
    ListView listview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_pharmacy);
        try {
            mClient = new MobileServiceClient(
                    "https://healthone.azurewebsites.net",
                    this
            );
        }catch (MalformedURLException me)
        {

            me.getStackTrace();
        }
        listview = (ListView) findViewById(R.id.pharmacyList);
        mPharmacyTable=mClient.getTable(Pharmacy.class);
        Intent intent= getIntent();
        patientID=intent.getStringExtra("patientID");
        getPharmacyName();





    }


    public void getPharmacyName(){
        try {
            mPharmacyTable.select("pharmacy_name")
                    .execute(new TableQueryCallback<Pharmacy>() {
                        public void onCompleted(List<Pharmacy> result,
                                                int count,
                                                Exception exception,
                                                ServiceFilterResponse response) {
                            if (exception == null) {
                                for (Pharmacy p : result) {
                                    pharmacyNames.add(p.getPharmacy_name());


                                }
                            } else {
                                Context context = getApplicationContext();
                                //   Toast.makeText(Doctor.this, "Ops!!! Error.", 1000).show();
                                Toast toast = Toast.makeText(context, "incorrect password", Toast.LENGTH_LONG);
                            }
                            display();}
                    });

        }catch (Exception e){
            Log.v("exception",e.getStackTrace().toString());
        }




    }

public void onSendClick(View view){
    Context context = getApplicationContext();
    int duration= Toast.LENGTH_SHORT;
    Toast.makeText(context, "Request Sent", duration).show();

}


    public void display(){
        itemsAdapter = new
                ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, pharmacyNames);
        //connect the listview and the adapter
        listview.setAdapter(itemsAdapter);
    }
}
