package com.example.healthone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableOperationCallback;

import java.net.MalformedURLException;
import java.util.Date;

/**
 * Created by Aditya on 27-09-2016.
 */
public class MainActivity extends Activity {

    final public int LOGIN_VARIABLE=101;
    final public int DOCTOR_HOME=102;
  // MobileServiceClient mClient;
 // MobileServiceTable<Patient_Visits> mDoctorTable;
    @Override
    public void onCreate(Bundle savedInstanceState){
     super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

     /*   try {
            mClient = new MobileServiceClient(
                    "https://healthone.azurewebsites.net",
                    this
            );
        }catch (MalformedURLException me)
        {
            me.getStackTrace();
        }*/
    /*    mDoctorTable = mClient.getTable("patient_visits",Patient_Visits.class);


        Patient_Visits item = new Patient_Visits();
        item.patient_id = "101";
       // item.address="25 Larkin St";
       // item.contact_number="0420427377";
       // item.first="sagar";
      //  item.last="Srinivas";
       // item.email="sagarsrinivas94@gmail.com";
        //item.password="qwerty";
        //item.blood_group="B";
        item.reason_for_visit="HIV";
        item.symptoms="Poor Immmune system";
        item.visit_date=new Date();
        item.doctor_id="1414153";

                    mClient.getTable(Patient_Visits.class).insert(item, new TableOperationCallback<Patient_Visits>() {
                        public void onCompleted(Patient_Visits entity, Exception exception, ServiceFilterResponse response) {
                            if (exception == null) {

                            } else {
                                // Insert failed
                    System.out.println(exception.getStackTrace());
                                System.out.println(exception.getMessage());
                }
            }
        });*/
 }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){


        if(requestCode==LOGIN_VARIABLE){
            if(resultCode==RESULT_OK){
                Intent intent=new Intent(this,DoctorHome.class);
                startActivityForResult(intent,DOCTOR_HOME);

            }
        }
    }

    public void onDoctorClick(View view){
        Intent intent= new Intent(MainActivity.this,DoctorLogIn.class);
        startActivityForResult(intent,LOGIN_VARIABLE);

    }

    public void onPatientClick(View view){
        Intent intent= new Intent(MainActivity.this,PatientLogIn.class);
        startActivity(intent);


    }

    public void onPharmaClick(View view){
        Intent intent=new Intent(this,PharmacyLogin.class);
        startActivity(intent);
    }




}
