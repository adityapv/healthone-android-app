package com.example.healthone;

/**
 * Created by Aditya on 27-09-2016.
 */
public class Pharmacy {
    String id;
    String pharmaccy_id;
    String pharmacy_name;
    String location;
    String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Pharmacy(String id, String pharmaccy_id, String pharmacy_name, String location, String password) {
        this.id = id;
        this.pharmaccy_id = pharmaccy_id;
        this.pharmacy_name = pharmacy_name;
        this.location = location;
        this.password = password;
    }

    public Pharmacy(String pharmaccy_id) {
        this.pharmaccy_id = pharmaccy_id;
    }

    public String getPharmacy_name() {
        return pharmacy_name;
    }

    public void setPharmacy_name(String pharmacy_name) {
        this.pharmacy_name = pharmacy_name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public Pharmacy(String id, String pharmaccy_id, String pharmacy_name, String location) {
        this.id = id;
        this.pharmaccy_id = pharmaccy_id;
        this.pharmacy_name = pharmacy_name;
        this.location = location;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPharmaccy_id() {
        return pharmaccy_id;
    }

    public void setPharmaccy_id(String pharmaccy_id) {
        this.pharmaccy_id = pharmaccy_id;
    }

    public Pharmacy(String pharmaccy_id, String pharmacy_name, String location) {
        this.pharmaccy_id = pharmaccy_id;
        this.pharmacy_name = pharmacy_name;
        this.location = location;
    }

    public Pharmacy() {
    }
}
