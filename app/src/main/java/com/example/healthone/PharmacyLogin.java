package com.example.healthone;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableQueryCallback;

import java.net.MalformedURLException;
import java.util.List;

/**
 * Created by Lohitt on 16-10-2016.
 */

public class PharmacyLogin extends Activity {
    EditText pharmacyIDET;
    EditText passwordET;
    String pharmacypwd;
    String pharmacyID;
    boolean flag=false;
    MobileServiceClient mClient;
    MobileServiceTable<Pharmacy> mPharmacyTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pharmacy_login);
        try {
            mClient = new MobileServiceClient(
                    "https://healthone.azurewebsites.net",
                    this
            );
        }catch (MalformedURLException me)
        {
            me.getStackTrace();
        }
        mPharmacyTable=mClient.getTable(Pharmacy.class);

    }


    public void onPharmacyLoginClick(View view) {
        pharmacyIDET = (EditText) findViewById(R.id.pharmacylogin1);
        passwordET = (EditText) findViewById(R.id.pharmacypassword);
        pharmacypwd = passwordET.getText().toString();
        pharmacyID = pharmacyIDET.getText().toString();


        mPharmacyTable.where().field("pharmaccy_id").eq(pharmacyID).select("password")
                .execute(new TableQueryCallback<Pharmacy>() {
                    public void onCompleted(List<Pharmacy> result,
                                            int count,
                                            Exception exception,
                                            ServiceFilterResponse response) {
                        if (exception == null) {
                            for (Pharmacy d : result) {

                                if (pharmacypwd.equals(d.password)) {
                                    flag = true;
                                    gotoNextPage();
                                    break;


                                }

                            }
                        } else {
                            Context context = getApplicationContext();
                            //   Toast.makeText(Doctor.this, "Ops!!! Error.", 1000).show();

                            System.out.println(exception.getStackTrace());
                            Toast toast = Toast.makeText(context, "incorrect password", Toast.LENGTH_LONG);
                        }
                    }
                });
    }
     public void gotoNextPage(){
        Intent intent1= new Intent(this,PharmacyHome.class);
        intent1.putExtra("pharmacyID",pharmacyID);
        setResult(RESULT_OK);
        startActivity(intent1);


        }






}
