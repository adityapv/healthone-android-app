package com.example.healthone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.EditText;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableOperationCallback;

import java.net.MalformedURLException;
import java.util.Date;

/**
 * Created by Aditya on 15-10-2016.
 */
public class DoctorNotes extends Activity {
    MobileServiceClient mClient;
    MobileServiceTable<Patient_Visits> mVisitTable;
    MobileServiceTable<Prescription> mPrescriptionTable;
    EditText diagnosis;
    EditText symptoms;
    EditText medicines;
    EditText instructions;
    String patientID;
    String doctorID;
    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.new_appointment);
        try {
            mClient = new MobileServiceClient(
                    "https://healthone.azurewebsites.net",
                    this
            );
        }catch (MalformedURLException me)
        {
            me.getStackTrace();
        }
        Intent intent=getIntent();
       patientID=intent.getStringExtra("patientID");
        doctorID=intent.getStringExtra("doctorID");
        mVisitTable=mClient.getTable("patient_visits",Patient_Visits.class);
        mPrescriptionTable=mClient.getTable(Prescription.class);
        diagnosis=(EditText) findViewById(R.id.reason);
        symptoms=(EditText) findViewById(R.id.symptoms1);
        medicines=(EditText) findViewById(R.id.medicines);
        instructions=(EditText) findViewById(R.id.instructions);



    }

    public void onNewEntryClick(View view){

        Patient_Visits item = new Patient_Visits();
        item.patient_id = "patientID";

        item.reason_for_visit=diagnosis.getText().toString();
        item.symptoms=symptoms.getText().toString();
        item.visit_date=new Date();
        item.doctor_id=doctorID;

        mVisitTable.insert(item, new TableOperationCallback<Patient_Visits>() {
            public void onCompleted(Patient_Visits entity, Exception exception, ServiceFilterResponse response) {
                if (exception == null) {

                } else {
                    // Insert failed
                    System.out.println(exception.getStackTrace());
                    System.out.println(exception.getMessage());
                }
            }
        });

        Prescription prescription=new Prescription();

        prescription.doctor_id=doctorID;
        prescription.medicine_id=medicines.getText().toString();
        prescription.intake_instructions=instructions.getText().toString();

       mPrescriptionTable.insert(prescription, new TableOperationCallback<Prescription>() {
            public void onCompleted(Prescription entity, Exception exception, ServiceFilterResponse response) {
                if (exception == null) {

                } else {
                    // Insert failed
                    System.out.println(exception.getStackTrace());
                    System.out.println(exception.getMessage());
                }
            }
        });
    }
}
